# cicd-with-robotframework

## Goal

* Build a robotframework docker container as a service
* execute a robot test suite
* add a feature to push tests to a test management system
* add a feature to pull tests from a test management system
* add a feature to perform headless GUI autoamted tests for chrome
* add a feature to perform headless GUI autoamted tests for firefox
* add a feature to perform headless GUI autoamted tests for edge

## Getting started: Building a RFW docker container

This can be performed using a CI/CD process.  The example here will be done using gitlab.com's internal project registry.  
FUTURE: setup recipie to push to other registries and build from other systems (such as Jenkins)  

### .gitlab-ci.yml Build container job

1.  Create a Directory in the repo that is the same name as the gitlab job
2.  template job for making a container
```yaml
my-container-service:
    before_script:
        # If you are using gitlab's built in container registry, credentials are part of the 
        # job instantiation and can be referenced with variables.
        - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
        # Whatever the name of the job, create a directory named the same way.
        # We will change to that directory
        # This would be the location of the Dockerfile used later to build the docker iamge.
        - cd $CI_JOB_NAME
        # call the docker image used to create other docker images
    image: docker:latest
    variables:
        # DEFINE HOW YOU WOULD LIKE TO NAME THE GENERATED CONTAINER
        FULL_NAME: $CI_REGISTRY_IMAGE/$CI_JOB_NAME
    stage: deploy
    services:
        - docker:dind
    script:
        - docker info
        - docker build --pull -t $FULL_NAME .
        - docker tag $FULL_NAME:latest $FULL_NAME:$CI_COMMIT_SHORT_SHA
        - docker push $FULL_NAME
    rules:
        - if: $CI_COMMIT_BRANCH == "main"
          changes:
            - my-container-service/*
```
